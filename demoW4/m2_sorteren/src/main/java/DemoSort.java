import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DemoSort {
    public static void main(String[] args) {
        String[] stringArray = {"Charlie", "Beta", "Alfa", "Delta", "Echo", "Foxtrot"};
        List<String> myList = new ArrayList<>(Arrays.asList(stringArray));

        System.out.println("alfabetisch:");
        //TODO
        drukOp1regel(myList);

        System.out.println("\nvolgens lengte:");
        //TODO
        drukOp1regel(myList);

        System.out.println("\nomgekeerd alfabetisch:");
        //TODO
        drukOp1regel(myList);

        System.out.println("\nvolgens laatste karakter:");
        //TODO
        drukOp1regel(myList);

    }
    private static void drukOp1regel(List<String> list) {
        for (String s : list) {
            System.out.print(s + " ");
        }
        System.out.println();
    }
}
